<?php

namespace App\Console\Commands;

use App\Jobs;
use Illuminate\Console\Command;

class ImagesStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show queues status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Jobs $failed)
    {
        list($resize, $upload, $processed, $failed) = $this->loadJobsStatus($failed);
        $this->info("Images Processor Bot");
        $this->comment("Queue   :  Count");
        $this->comment("resize  :  " . count($resize));
        $this->comment("upload  :  " . count($upload));
        $this->comment("done    :  " . count($processed));
        $this->comment("failed  :  " . count($failed));
    }

    /**
     * @param Jobs $failed
     * @return array
     */
    protected function loadJobsStatus(Jobs $failed)
    {
        $data = $failed->all()->groupBy('queue');
        $resize = isset($data[Jobs\ImageResize::QUEUE])
            ? $data[Jobs\ImageResize::QUEUE]
            : [];
        $upload = isset($data[Jobs\ImageUpload::QUEUE])
            ? $data[Jobs\ImageUpload::QUEUE]
            : [];
        $processed = isset($data[Jobs\ImageProcessed::QUEUE])
            ? $data[Jobs\ImageProcessed::QUEUE]
            : [];
        $failed = $this->laravel['queue.failer']->all();

        return array($resize, $upload, $processed, $failed);
    }
}
