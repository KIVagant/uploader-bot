<?php

namespace App\Console\Commands;

use App\Jobs\ImageResize;
use Illuminate\Console\Command;
use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ImagesResize extends Command
{
    use DispatchesJobs;
    const DEFAULT_LIMIT = null;
    const DEFAULT_OFFSET = 0;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:resize
                            {src : Source directory}
                            {dest? : Destination directory}
                            {--O|--offset=}
                            {--L|--limit=}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push images from source folder to resizing and uploading queues';
    /**
     * @var FilesystemManager
     */
    protected $manager;
    protected $src;
    protected $dest;
    protected $offset;
    protected $limit;

    /**
     * Create a new command instance.
     *
     * @param FilesystemManager $manager
     */
    public function __construct(FilesystemManager $manager)
    {
        parent::__construct();
        $this->manager = $manager;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->prepareParams();
        $files = $this->manager->disk()->files($this->src);
        $count = $offsetCount =0;
        foreach ($files as $file) {
            $basename = basename($file);
            if ($offsetCount++ < $this->offset || $basename === '.gitignore') {
                continue;
            }
            $this->comment('From ' . $file . ' to ' . $this->dest . $basename);
            $this->scheduleJob($file, ($this->dest ? $this->dest . $basename : null));
            $count++;
            if ($this->limit && $count === $this->limit) {
                break;
            }
        }
        $this->info($count . ' images added to queue');
    }

    /**
     * @param string $src
     * @param string $dest
     */
    protected function scheduleJob($src, $dest)
    {
        $job = new ImageResize($src, $dest);
        $this->dispatch($job);
    }

    protected function prepareParams()
    {
        $this->src = $this->argument('src');
        $this->dest = $this->argument('dest') ?? null;
        $this->limit = (int)$this->option('limit') ?? self::DEFAULT_LIMIT;
        $this->offset = (int)$this->option('offset') ?? self::DEFAULT_LIMIT;
    }
}
