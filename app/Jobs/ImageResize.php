<?php

namespace App\Jobs;

use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ImageResize extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    const QUEUE = 'images.resize';
    const DEFAULT_DESTINATION = './images_resized/';

    const BACKGROUND_COLOR = 'rgb(255, 255, 255)';
    const RESIZE_WIDTH = 640;
    const RESIZE_HEIGHT = 640;
    const RESIZE_FORMAT = 'jpg';

    /**
     * @var string
     */
    protected $src;
    /**
     * @var string
     */
    protected $dest;

    /**
     * @return string
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * @return string
     */
    public function getDest()
    {
        return $this->dest;
    }

    /**
     * @param string $src Source image
     * @param string $dest Destination image
     */
    public function __construct($src, $dest = null)
    {
        $this->queue = self::QUEUE;
        $this->src = $src;
        $this->dest = $dest ?? self::DEFAULT_DESTINATION . basename($src);
    }

    /**
     * Execute the job.
     *
     * @param FilesystemManager $manager
     */
    public function handle(FilesystemManager $manager)
    {
        if ($manager->disk()->exists($this->src)) {
            $this->thumbnailImage($manager);
            $manager->disk()->delete($this->src);
        }
    }

    /**
     * Handle a job failure.
     *
     * @return void
     */
    public function failed()
    {
        // Called when the job is failing...
    }

    protected function getImagick()
    {
        if (!class_exists(\Imagick::class)) {
            throw new \RuntimeException('Imagick is not installed', __LINE__);
        }

        return new \Imagick();
    }

    protected function thumbnailImage(FilesystemManager $manager)
    {
        $imagick = $this->getImagick();
        $readStream = $manager->disk()->readStream($this->src);
        $imagick->readImageFile($readStream);
        $imagick->setImageBackgroundColor(self::BACKGROUND_COLOR);
        $imagick->thumbnailImage(self::RESIZE_WIDTH, self::RESIZE_HEIGHT, true, true);
        $handle = fopen('php://temp', 'w+');
        $imagick->writeImageFile($handle);
        $manager->disk()->writeStream($this->dest, $handle);
        fclose($handle);
        fclose($readStream);
    }
}
