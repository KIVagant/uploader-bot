<?php

namespace App\Jobs;

use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ImageProcessed extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    const QUEUE = 'images.processed';

    /**
     * @var string
     */
    protected $src;

    /**
     * @return string
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * @param string $src Source path
     */
    public function __construct($src)
    {
        $this->queue = self::QUEUE;
        $this->src = $src;
    }

    /**
     * Execute the job.
     *
     * @param FilesystemManager $manager
     */
    public function handle(FilesystemManager $manager)
    {
        if ($manager->disk()->exists($this->src)) {
            // Add your logic here...
        }
    }

    /**
     * Handle a job failure.
     *
     * @return void
     */
    public function failed()
    {
        // Called when the job is failing...
    }
}