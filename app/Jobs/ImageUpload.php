<?php

namespace App\Jobs;

use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ImageUpload extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    const DEFAULT_DESTINATION = './images_uploaded/';
    const QUEUE = 'images.upload';
    /**
     * @var string
     */
    protected $src;
    /**
     * @var string
     */
    protected $dest;
    /**
     * @var
     */
    protected $destDisk;

    /**
     * @return string
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * @return string
     */
    public function getDest()
    {
        return $this->dest;
    }

    /**
     * @param string $src Source path
     * @param null $destDisk Set the destination disk with filesystem
     * @param null $dest
     */
    public function __construct($src, $destDisk = null, $dest = null)
    {
        $this->queue = self::QUEUE;
        $this->src = $src;
        $this->dest = $dest ?? self::DEFAULT_DESTINATION . basename($src);
        $this->destDisk = $destDisk;
    }

    /**
     * Execute the job.
     *
     * @param FilesystemManager $manager
     */
    public function handle(FilesystemManager $manager)
    {
        if ($manager->disk()->exists($this->src)) {
            $stream = $manager->disk()->readStream($this->src);
            $manager->disk($this->destDisk)->put($this->dest, $stream);
            fclose($stream);
        }
    }

    /**
     * Handle a job failure.
     *
     * @return void
     */
    public function failed()
    {
        // Called when the job is failing...
    }
}