<?php

return [

    // Setup the destination disk for uploaded files
    'upload_disk' => env('STORAGE_DISK_UPLOAD', 'local'),
];
