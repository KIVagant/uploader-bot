# Uploader Bot

## Dependencies

- laravel/homestead – Virtual development environment

## Installation

- Clone this repo.
- Add the line: ```192.168.10.10 uploader-bot.dev``` to the ```/etc/hosts```
- Change project folders in this file: ```Homestead.yaml```.
- Copy ```.env.example``` to ```.env```

By default, two local storage adapters used: 'local' and 'public'.
First of all, put any images to ```storage/app/images``` folder.
Resized images will be in ```storage/app/images```.

Uploaded images will be in ```storage/app/public/images_uploaded```
or in ```storage/app/images_uploaded``` (depends on you .env settings).

You can change the ```config/filesystems.php``` and configure many another adapters including clouds.
See [flysystem documentation](http://flysystem.thephpleague.com/) for details.
Then change this variables in .env file:

- ```STORAGE_DISK_DEFAULT=local```
- ```STORAGE_DISK_UPLOAD=public```

Run:

```
composer install
vagrant up
vagrant ssh
cd ~/uploader-bot
composer install # repeat this command inside box for stability improvements
bot
```

## Usage

```
    command [arguments]
Available commands:
    schedule    Add filenames to resize queue
                    -s, --source
                    [-o, --offset]
                    [-n, --count]
    resize      Resize next images from the queue
                    [-n, --count]
                    [-t, --tries]
    upload      Upload next images to remote storage
                    [-n, --count]
                    [-t, --tries]
    status      Output current status in format %queue%:%number_of_images%
    failed      Show all failed job
    retry       Retry failed job
    flush       Delete all failed job
```

## Examples

```
bot schedule -s=./images -n=2
bot schedule -s=./images -o=3 -n=2
bot schedule -s=./images -o=6
bot status
bot resize -n=3
bot resize
bot status
bot upload -n=5
bot upload
bot status
```

## License
This code made just for skills testing and licensed under MIT license.