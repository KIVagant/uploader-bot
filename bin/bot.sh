#!/usr/bin/env bash
help="Uploader Bot
Usage:
    command [arguments]
Available commands:
    schedule    Add filenames to resize queue
                    -s, --source
                    [-o, --offset]
                    [-n, --count]
    resize      Resize next images from the queue
                    [-n, --count]
                    [-t, --tries]
    upload      Upload next images to remote storage
                    [-n, --count]
                    [-t, --tries]
    status      Output current status in format %queue%:%number_of_images%
    failed      Show all failed job
    retry       Retry failed job
    flush       Delete all failed job
";
P_ROOT="/home/vagrant/uploader-bot/"
A_COMMAND=$1;
for i in "$@"
do
case $i in
    -h|--help)
    echo "${help}"
    shift
    exit;
    ;;
    -s=*|--source=*)
    A_SRC="${i#*=}"
    shift
    ;;
    -t=*|--tries=*)
    A_TRIES="${i#*=}"
    shift
    ;;
    -n=*|--count=*)
    A_COUNT="${i#*=}"
    shift
    ;;
    -o=*|--offset=*)
    A_OFFSET="${i#*=}"
    shift
    ;;
esac
done
function exe {
    echo "\$ $@" ;
    eval ${@} ;
}

if [[ -z ${A_COMMAND} ]]; then
    echo "${help}"
    exit 1
fi

if [ "${A_COMMAND}" = "schedule" ]; then
    if [[ -z ${A_SRC} ]]; then
        echo "Missed src_dir argument"
        exit 1
    fi
    if [[ -z ${A_COUNT} ]]; then
        A_COUNT="0"
    fi
    if [[ -z ${A_OFFSET} ]]; then
        A_OFFSET="0"
    fi
    exe "php ${P_ROOT}artisan images:resize ${A_SRC} -O ${A_OFFSET} -L ${A_COUNT}"
fi
if [[ -z ${A_TRIES} ]]; then
    A_TRIES="1"
fi
if [ "${A_COMMAND}" = "resize" ]; then
    if [[ -z ${A_COUNT} ]]; then
        exe "php ${P_ROOT}artisan queue:listen --queue=images.resize --tries=${A_TRIES}"
    fi
    for (( i = 1; i <= ${A_COUNT}; i++ )); do
        echo "${i} / ${A_COUNT}"
        exe "php ${P_ROOT}artisan queue:work --queue=images.resize --tries=${A_TRIES}"
    done
fi
if [ "${A_COMMAND}" = "upload" ]; then
    if [[ -z ${A_COUNT} ]]; then
        exe "php ${P_ROOT}artisan queue:listen --queue=images.upload --tries=${A_TRIES}"
    fi
    for (( i = 1; i <= ${A_COUNT}; i++ )); do
        echo "${i} / ${A_COUNT}"
        exe "php ${P_ROOT}artisan queue:work --queue=images.upload --tries=${A_TRIES}"
    done
fi
if [ "${A_COMMAND}" = "retry" ]; then
    exe "php ${P_ROOT}artisan queue:retry all"
fi
if [ "${A_COMMAND}" = "failed" ]; then
    exe "php ${P_ROOT}artisan queue:failed"
fi
if [ "${A_COMMAND}" = "flush" ]; then
    exe "php ${P_ROOT}artisan queue:flush"
fi
if [ "${A_COMMAND}" = "status" ]; then
    exe "php ${P_ROOT}artisan images:status"
fi

